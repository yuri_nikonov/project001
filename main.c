
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "main.h"

	double duration, duration_prev;
	clock_t startClock;

void duration_calc()
{
	duration_prev = duration;
	duration = (clock() - startClock) / (double)CLOCKS_PER_SEC;
	fprintf(stdout, "Delta duration: %4.3f sec., duration: %4.3f sec.: ", duration - duration_prev, duration );
}


int main()
{
	struct ListItem {
		int unsigned index;
		char name[MAXLEN];
		struct ListItem *next;
	} *head = NULL, *current, *prev, *prevcurrent;
	
	union random4 {
		int unsigned intvalue;
		char unsigned charvalue[4];
	};
	
	FILE *fp;
	int unsigned i, j, szListItem, timestart;
	startClock = clock();
	srandom(time(NULL));
	szListItem = sizeof(struct ListItem);
	
	fp = fopen("outfile.dat", "w");
	
	for (i = 0; i < ITEMS; i++)
	{
		prev = current;
		union random4 rnd;
		current = malloc(szListItem);
		if (!head)
			head = current;
		else
			prev->next = current;
		for (j = 0; j < MAXLEN - 1; j++)
		{
			if (!(j % 4))
				rnd.intvalue = random();
			current->name[j] = ((rnd.charvalue[j % 4] % 95) + 0x20);
		}
		current->name[MAXLEN - 1] = 0x00;
		current->index = i;
	}
	current->next = NULL;
	duration_calc();
	fprintf(stdout, "List of strings successfully created.\n");
	
	current = head;
	prevcurrent = head;
	i = 0;
	while (current)
	{
		struct ListItem *aftercurrent, *minitem, *prevminitem;
		if (!(i % 10000))
		{
			duration_prev = duration;
			duration_calc();
			fprintf(stdout, "%d/%d completed\n", i, ITEMS);
		}
		aftercurrent = current->next;
		minitem = current;
		prev = current;
		while (aftercurrent)
		{
			if (strncmp(aftercurrent->name, minitem->name, MAXLEN - 1) < 0)
			{
				minitem = aftercurrent;
				prevminitem = prev;
			}
			prev = aftercurrent;
			aftercurrent = aftercurrent->next;
		}
		if (current != minitem)
		{
			// current <-> minitem
			if (prevcurrent != current)
			{
				prevcurrent->next = minitem;
			}
			else
			{
				head = minitem;
			}
			prevminitem->next = current;
			prev = minitem->next;
			minitem->next = current->next;
			current->next = prev;
			current = minitem;
		}
		prevcurrent = current;
		current = current->next;
		i++;
	}
	duration_calc();
	fprintf(stdout, "Sorting list completed\n");
	current = head;
	prev = current;
	i = 0;
	while (current)
	{
		fprintf(fp, "%06d\t%s\n", current->index, current->name);
		if (strncmp(prev->name, current->name, MAXLEN - 1) > 0)
		{
			fprintf(stdout, "Error in sort order:\nPosition: %d \"%s\"\nPosition %d \"%s\"\n", i - 1, prev->name, i, current->name);
		}
		prev = current;
		current = current->next;
		i++;
	}
	duration_calc();
	fprintf(stdout, "Output list to file completed\n");
	
	while (head)
	{
		current = head;
		head = head->next;
		free(current);
	}
	duration_calc();
	fprintf(stdout, "Destroying of list completed, exiting\n");
	fclose(fp);
	duration_calc();
	putchar('\n');
	return 0;
}
