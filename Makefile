
main: main.o
	cc -o main main.o

main.o: main.c main.h
	cc -c -o main.o main.c

clean:
	rm main main.o outfile.dat


